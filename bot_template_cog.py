################################################################################################################################
# This is a template for an extension which is a seperate file from you bot's main one that holds commands and event listeners.#
# Cogs are what hold those commands and event listeners which are represented by Python classes.                               #
# (You can also have commands and event listeners but I'll show you what I think it's the easiest way of doing them)           #
# There are some things that might seem voodoo magic for people that never looked into the discord.py code, but it works like  #
# this: discord.py will look for any function that is called 'on_<something>' nad it will try to create a listener if it finds #
# a valid event.                                                                                                               #
# All the 'bot.add_cog()' method does is look for commands and valid events inside the specified class. And so, because that   # 
# does a lot of stuff in the background it might look voodoo type stuff. For a more detailed explanation ask for it in the     #
# #py-help channel at the support server given in the README.md file.                                                          #
# So using this method, in the main file you would do                                                                          #
#                                                                                                                              #
# @bot.listen()                                                                                                                #
# async def on_message(message):                                                                                               #
#   ...                                                                                                                        #
#                                                                                                                              #
# in a cog you do,                                                                                                             #
#                                                                                                                              #
# async def on_message(self, message):                                                                                         #
#   ...                                                                                                                        #
#                                                                                                                              #
# As you can see, there's no decorator for listener nor event (you shouldn't be using @bot.event in 99% the cases anyway)      #
# It's also worth mentioning that in all functions inside a cog (a Python class) need the "self" argument, if they are not a   #
# staticmethod or a classmethod.                                                                                               #
# If you don't know what I'm talking about I strongly advise you go read some online tutorials or the official documentations. #
################################################################################################################################





# Basic imports
import discord
from discord.ext import commands
import json

# Opening a JSON file with some basic data
with open('BOTBASICDATA.json') as json_fp:
    basic_data = json.load(json_fp) # Decoding data from the json file
    prefix = basic_data['prefix'] # Don't forget to check my BOTBASICDATA.json template file


class YourCogName:
    def __init__(self, bot):
        self.bot = bot


    #########################################################################################
    # Your commands here.                                                                   #
    # Remmeber to user '@commands.command' instead of '@bot.commands'.                      #
    # Don't forget the 'self' argument if you are not in a staticmethod or in a classmethod #
    #########################################################################################


def setup(bot):
    """Setting up all cogs in this extension this extension"""
    bot.add_cog(YourCogName(bot))
