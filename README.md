# discord.py bot template [REWRITE]

This is a basic template for discord.py that can be really helpful for starters with this wrapper. 

### Features

Let me just run down on what this template has to offer.

* Logging of boot-up activity which can be really useful for detecting errors

* A template for a handy json file where you can store your important data, so that you don't *(or at least you have less change to)* upload your token.

* A fully customizable *(of course)* activity changer for your bot. Make it play, listen, watch or stream whatever you desire.

* A beatiful and info filled ready message.

* A easy way of knowing if your bot booted-up properly or not by sending a message to you or to a guild channel.

* A template for making extensions and cogs, that includes the json file storage.

- And of course all of this is well documented for easy reading and comprehension. 

**[NOTE]:** If you are still using discord.py async I strongly advise you to upgrade to rewrite. The creator of discord.py does so too. It's poorly documented, feature limited and the API used for it will be closed soon, and so there's no real support/help for it. For more info join [this](https://discord.gg/GWdhBSp) server or the official discord.py one.
